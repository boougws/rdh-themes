<?php
/**
 * BWS/RedDeHogar 
 *
 * @package RedDeHogar
 */
 
 show_admin_bar(false);
 

/**
 * Enqueue scripts and styles
 */
function main_scripts() {
	
	// STYLES
	
	wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css', array(), null  );
	
 	wp_enqueue_style('bootstrap-style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), null  );

 	wp_enqueue_style('page-styles', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), null  );
 	
 	// SCRIPTS
 	
 	wp_enqueue_script('modernizer', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'));
 	
 	wp_enqueue_script('page-init', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'));

}

add_action( 'wp_enqueue_scripts', 'main_scripts' );