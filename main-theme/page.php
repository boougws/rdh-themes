<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Page</title>
	
		<meta name="generator" content="boougws.com" />
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!--<link rel="shortcut icon" href="">-->
		<meta property="og:url" content="http://reddehogar.com" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Red de Hogar" />
		<meta property="og:description" content="" />
	
		<!-- Google Fonts -->
		<link class="gf-headline" href='https://fonts.googleapis.com/css?family=Pacifico:400&subset=' rel='stylesheet' type='text/css'>
	
		<?php
			wp_head();
		?>
	
	</head>
	<body>
		<?php 
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post(); 
					//
					// Post Content here
					//
					the_title();
					the_content();
				} // end while
			} // end if
		?>
	</body>
</html>