<!DOCTYPE html>
<html class="cspio">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Estamos en Construcción | Red de Hogar</title>
	
		<meta name="generator" content="boougws.com" />
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!--<link rel="shortcut icon" href="">-->
		<meta property="og:url" content="http://reddehogar.com" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Estamos en construccion." />
		<meta property="og:description" content="" />
	
		<!-- Google Fonts -->
		<link class="gf-headline" href='https://fonts.googleapis.com/css?family=Pacifico:400&subset=' rel='stylesheet' type='text/css'>
	
		<?php
			wp_head();
		?>
	
	</head>
	<body>
		<div id="cspio-page">
			<div id="cspio-content">
				
				<img id="cspio-logo" src="<?php echo get_stylesheet_directory_uri() . '/assets/img/logo.png'; ?>"></img>
				
				<h1 id="cspio-headline">Estamos en Construcción</h1>	
				
				<div id="cspio-socialprofiles">
					
					<a href="https://www.facebook.com/reddehogar" target="_blank">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle-thin fa-stack-2x"></i>
							<i class="fa fa-facebook fa-stack-1x"></i>
						</span>
					</a>	
					
					<a href="https://www.instagram.com/reddehogar" target="_blank">
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle-thin fa-stack-2x"></i>
							<i class="fa fa-instagram fa-stack-1x"></i>
						</span>
					</a>			
				</div>
			</div>
		</div>
	</body>
</html>

